from unittest import TestCase

from challenge1.string_compression import compress


class Test(TestCase):
    def test(self):
        self.assertEqual(compress('bbcceeee'), 'b2c2e4')
        self.assertEqual(compress('aaabbbcccaaa'), 'a3b3c3a3')
        self.assertEqual(compress('a'), 'a')
        self.assertEqual(compress('ab'), 'ab')
        self.assertEqual(compress('abb'), 'ab2')
        self.assertEqual(compress('aab'), 'a2b')
        self.assertEqual(compress('1'), '1')
        # The next two examples show that compress is useless for compressing string with numbers
        self.assertEqual(compress('11'), '12')
        self.assertEqual(compress('12'), '12')
        self.assertEqual(compress('1111'), '14')


if __name__ == '__main__':
    print('Testing string_compression')
    Test().test()
