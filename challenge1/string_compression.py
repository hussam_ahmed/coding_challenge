def compress(s):
    """
    :param s: string to be compressed
    :return: compressed string. e.g. aaaabbcc > a4b2c2
    """
    result = ''
    current_char = ''
    count = 0
    for i in range(len(s)):
        char = s[i]
        if char != current_char:
            if count > 1:
                result += str(count)
            current_char = char
            result += current_char
            count = 1
            continue
        count += 1
    if count > 1:
        result += str(count)
    return result
