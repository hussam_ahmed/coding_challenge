class _Node(object):
    def __init__(self, name):
        if type(name) != str:
            raise TypeError('Nodes should have a name that\'s of type str')  # perhaps ints should also be allowed
        self.name = name


class _Link(object):
    def __init__(self, from_node, to_node):
        if type(from_node) != _Node:
            raise TypeError('Link should be initialized with nodes')
        if type(to_node) != _Node:
            raise TypeError('Link should be initialized with nodes')
        if from_node == to_node:
            raise ValueError('A node cannot link to itself')  # or can it? design decision
        self.from_node = from_node
        self.to_node = to_node


class DirectedGraph(object):
    def __init__(self, nodes, links):
        assert len(nodes) == len(set(nodes))
        self.nodes_dict = {}
        for node in nodes:
            self.nodes_dict[node] = _Node(node)
        self.links = []
        for link_from, link_to in links:
            self.links.append(_Link(self.nodes_dict[link_from], self.nodes_dict[link_to]))

    def identify_routers(self):
        """
        :return: names of nodes that have maximum inbound and outbound links
        """
        assert type(self) == DirectedGraph
        counts = {}
        for node in self.nodes_dict.keys():
            counts[node] = 0
        for link in self.links:
            counts[link.from_node.name] += 1
            counts[link.to_node.name] += 1
        if not counts:
            return []
        maximum = max(counts.values())
        result = []
        for k, v in counts.items():
            if v == maximum:
                result.append(k)
        return result
