from unittest import TestCase

from challenge1.network_failure_point import DirectedGraph


class Test(TestCase):
    def test1(self):
        graph = DirectedGraph(
            nodes=['1', '2', '3', '4', '5', '6'],
            links=[
                ('1', '2'),
                ('2', '3'),
                ('3', '5'),
                ('5', '2'),
                ('2', '1'),
            ])
        self.assertEqual(graph.identify_routers(), ['2'])

    def test2(self):
        graph = DirectedGraph(
            nodes=['1', '2', '3', '4', '5', '6'],
            links=[
                ('1', '3'),
                ('3', '5'),
                ('5', '6'),
                ('6', '4'),
                ('4', '5'),
                ('5', '2'),
                ('2', '6'),
            ])
        self.assertEqual(graph.identify_routers(), ['5'])

    def test3(self):
        graph = DirectedGraph(
            nodes=['1', '2', '3', '4', '5', '6'],
            links=[
                ('2', '4'),
                ('4', '6'),
                ('6', '2'),
                ('2', '5'),
                ('5', '6'),
            ])
        result = graph.identify_routers()
        result.sort()
        self.assertEqual(result, ['2', '6'])

    def test4(self):
        with self.assertRaises(ValueError):
            DirectedGraph(
                nodes=['1', '2', '3', '4', '5', '6'],
                links=[
                    ('1', '3'),
                    ('3', '3'),
                ])

    def test5(self):
        with self.assertRaises(TypeError):
            DirectedGraph(
                nodes=[1, 2, 3, 4, 5, 6],
                links=[
                    ('1', '3'),
                ])

    def test6(self):
        with self.assertRaises(KeyError):
            DirectedGraph(
                nodes=['1', '2', '3', '4', '5', '6'],
                links=[
                    ('1', '7'),
                ])

    def test7(self):
        graph = DirectedGraph(
            nodes=['1', '2', '3', '4', '5', '6'],
            links=[
                ('1', '2'),
                ('1', '6'),
                ('1', '4'),
                ('1', '5'),
                ('1', '6'),
                ('1', '6'),
                ('1', '6'),
                ('2', '3'),
                ('2', '4'),
                ('2', '5'),
                ('2', '6'),
                ('6', '2'),
                ('6', '2'),
            ])
        result = graph.identify_routers()
        result.sort()
        self.assertEqual(result, ['1', '2', '6'])

    def test8(self):
        graph = DirectedGraph(
            nodes=['1', '2', '3', '4', '5', '6'],
            links=[
                ('1', '2'),
                ('1', '3'),
                ('1', '4'),
                ('1', '5'),
                ('1', '6'),
                ('1', '5'),
                ('1', '4'),
                ('1', '3'),
                ('1', '2'),
                ('2', '1'),
                ('3', '1'),
                ('4', '1'),
                ('5', '1'),
                ('6', '1'),
                ('5', '1'),
                ('4', '1'),
                ('3', '1'),
                ('2', '1'),
            ])
        self.assertEqual(graph.identify_routers(), ['1'])

    def test9(self):
        # TODO: in this case should we return everything or nothing?
        graph = DirectedGraph(
            nodes=['1', '2', '3', '4', '5', '6'],
            links=[])
        self.assertEqual(graph.identify_routers(), ['1', '2', '3', '4', '5', '6'])

    def test10(self):
        graph = DirectedGraph(
            nodes=[],
            links=[])
        self.assertEqual(graph.identify_routers(), [])


if __name__ == '__main__':
    test_instance = Test()
    for i in range(100):
        if hasattr(test_instance, 'test' + str(i)):
            print('Testing network_failure_point -- test case: ' + str(i))
            test_function = getattr(test_instance, 'test' + str(i))
            test_function()
