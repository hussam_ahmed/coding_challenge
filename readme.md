# Challenge 1

## String compression

The complexity of the `compress` function is linear with the length of string `s`.

## Network failure point

The complexity of the `identify_routers` function is linear with the number of links in the graph
assuming the number of links in the graph >> the number of nodes in the graph, otherwise its is linear
with the number of nodes although this could be eliminated by skipping looping over the nodes to initialize
`counts` but we keep it this way as it is more elegant and optimize later on!


# Challenge 2

ATM, what we have is just a proof of concept. I'll be honest this is the first time I've used
Stripe, but it seems brilliant and easy to use. Anyway, here's a list of things needed to be completed,
which I'll hopefully get to during this weekend if you're still interested.

- Use a postgres db instead of sqlite
- Add a cancel membership view or functionality  
- Create a subscription model and track subscriptions with the help of the Stripe API (needs some digging into stripe docs)
- Pass sensitive values, or values that change between production and development in environment values

## Product

![Product as appears in Stripe](product.png)
