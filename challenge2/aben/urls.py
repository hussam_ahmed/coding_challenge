from django.urls import path

from .views import index
from .views import signup

app_name = "aben"

urlpatterns = [
    path('',        index,  name='index'),
    path('signup',  signup, name='signup'),
]
