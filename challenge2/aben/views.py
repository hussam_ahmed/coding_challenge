from django.shortcuts import render, redirect
from django.urls import reverse
import stripe


publik_key = 'pk_test_51IOrrCC0Xb5EnDwFMieo7n3I5dCkgf1aCVcjWuC9PjkPy8z8Qyz35T' \
             '7a9toQ5unMRzuGIIdkiF9YpYpeJfBh7Scc00L5jpN285'


def index(request):
    if not request.user or request.user.is_anonymous:
        return redirect(reverse('django_registration_register'))

    stripe.api_key = 'sk_test_51IOrrCC0Xb5EnDwFzPRBSm5yhYzdHBJ0BqYYvQZXuIOPdJM' \
                     'JEeCzrhEcddJ2M7S5VlwzMoIkErn4AmFE8TVAIye200bWpMAdkw'

    session = stripe.checkout.Session.create(
        payment_method_types=['card'],
        line_items=[{
            'price': 'price_1IOt2FC0Xb5EnDwFZ1TSo3X5',
            'quantity': 1,
        }],
        mode='subscription',
        success_url=request.build_absolute_uri(reverse('aben:signup')) + '?user_id=' + str(request.user.id),
        cancel_url=request.build_absolute_uri(reverse('aben:index')),
    )
    session_id = session.id

    return render(request=request, template_name="index.html", context={
        'session_id': session_id,
        'public_key': publik_key,
    })


def signup(request):
    if not request.user or request.user.is_anonymous:
        return redirect(reverse('django_registration_register'))
    if request.method == 'POST':
        print('Tralala')
    return render(request=request, template_name="index.html", context={})
