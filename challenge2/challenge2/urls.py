from django.contrib import admin
from django.urls import path
from django.urls import include
from django_registration.backends.one_step.views import RegistrationView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/register/', RegistrationView.as_view(success_url='/'), name='django_registration_register'),
    path('', include('aben.urls', namespace='aben')), ]
